# -*- coding: utf-8 -*-
from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond import backend
import csv
import codecs
import os
__all__ = ['Product']

class Product(metaclass = PoolMeta):
    __name__ = 'product.product'
    
    @classmethod
    def __register__(cls, module_name):
        #print(str(os.path.dirname(os.path.abspath(__file__))))        
        super(Product, cls).__register__(module_name)
        TableHandler = backend.get('TableHandler')
        table = TableHandler(cls, module_name)
        sql_table = cls.__table__()
        
        pool = Pool()
        Products = pool.get('product.product')
        Templates = pool.get('product.template')
        template_table = Templates.__table__()
        Medicaments = pool.get('gnuhealth.medicament')
        medicament_table = Medicaments.__table__()
        cursor = Transaction().connection.cursor()
        ### replace old remediar names with the new names on the csv file,
        ### deleting new register created by the xml files on data folder
        with open(str(os.path.dirname(os.path.abspath(__file__)))+'/scripts/remediar_list.csv',
                  newline='', encoding='latin1') as csvfile:
            source = csv.reader(csvfile, delimiter=':')
            
            for product_name in source:
                old_product_code = product_name[0][0:4]
                new_template_name = product_name[0]                
                
                #grab old ids from product_product                
                old_product = Products.search([
                    ('code','=',old_product_code)])
                old_product_id = old_product and old_product[0].id or 0 
                old_product_template_id = old_product and old_product[0].template.id or 0
                
                #grab old ids from gnuhealth_medicine
                old_medicament = Medicaments.search([
                    ('name','=',old_product_id)])
                old_medicament_id = old_medicament and old_medicament[0].id or 0
                
                #grab old ids from product_template
                old_template = Templates.search([
                    ('id','=',old_product_template_id)])
                old_template_id = old_template and old_template[0].id or 0
                                
                #grab new ids from product_template
                new_template = Templates.search([
                    ('name','=',new_template_name)])
                new_template_id = new_template and new_template[0].id or 0
                                
                #grab new ids from product_product with the new template_ids
                new_product = Products.search([
                    ('template','=',new_template_id)])
                new_product_id = new_product and new_product[0].id or 0
                               
                #grab new ids from gnuhealth_medicine with the product_ids
                new_medicament = Medicaments.search([
                    ('name','=',new_product_id)])
                new_medicament_id = (new_medicament and new_medicament[0].id) or 0
                if (new_medicament_id !=0) and new_medicament[0].category:
                    new_medicament_category_id = new_medicament[0].category.id
                else: 
                    0

                #if the new_product_id is new, it will point
                #the registers of old products on product_product to the new templates
                if (old_product_id != new_product_id) and (old_product_id != 0) and (new_template_id !=0):
                    print('\n Actualizando Productos \n')
                    cursor.execute(*sql_table.update(
                        columns=[sql_table.template,sql_table.code],
                        values=[new_template_id,None],
                        where=sql_table.id==old_product_id))                    
                   
                    #point old_medicament to a new_medicament_category and eliminates new_medicament
                    if new_medicament_id!=0:
                        print('\n Actualizando Medicamentos y eliminando nuevos medicamentos\n')
                        cursor.execute(*medicament_table.update(
                            columns=[medicament_table.category],
                            values=[new_medicament_category_id],
                            where=medicament_table.name==old_product_id))
