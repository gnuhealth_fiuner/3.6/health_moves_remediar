# -*- coding: utf-8 -*-
from trytond.report import Report

__all__ = ['ReportMovimientos']

class ReportMovimientos(Report):
    __name__ = 'gnuhealth.report.movimientos'
